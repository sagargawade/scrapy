import scrapy
from ..items import LivemintItem

class QuoteSpider(scrapy.Spider):
    name = 'livemint'
    page_number = 2
    page_number1 = 2
    page_number2 = 2
    page_number3 = 2
    start_urls = [
        'https://mag.wodrob.com/'
    ]

    def parse(self, response):
        print("Inside Parse",response)
        for url in response.css('header a')[3:7].xpath('@href').extract():
            yield scrapy.Request(url ='https://mag.wodrob.com' + url, callback=self.extract)

            
    def extract(self, response):
        print("Inside extract",response)
        for link in response.css('article a.featured-img-container').xpath("@href").extract():
            # print('link',link)
            yield scrapy.Request(url = link, callback=self.parse_events, dont_filter=True)


    def parse_events(self,response):
        print("inside method")
        items = LivemintItem()

        all_div = response.css('#content')
        image = all_div.css("header div.featured-img-container").xpath('@style').extract_first().replace("background-image:url(", "").replace(");filter:blur(5px)","").replace("background-image: url(", "").replace(");","").replace(")","")
        # print('image',image)
        title = all_div.css("header h1::text").extract()
        # print('title',title)
        description = all_div.css("article p::text").extract()
        # print('description',description)
        author = all_div.css("header div.meta-item a::text")[0].extract()
        # print('author',author)
        date = all_div.css("header div.meta-item time::text")[0].extract()
        # print('date',date)

        items['image'] = image
        items['title'] = title
        items['description'] = description
        items['author'] = author
        items['date'] = date

        yield items

        next_page = 'https://mag.wodrob.com/trends/page/' + str(QuoteSpider.page_number)
        if QuoteSpider.page_number <= 7:
            QuoteSpider.page_number += 1
            yield response.follow(next_page, callback = self.extract)

        next_page = 'https://mag.wodrob.com/style/page/' + str(QuoteSpider.page_number1)
        if QuoteSpider.page_number1 <= 21:
            QuoteSpider.page_number1 += 1
            yield response.follow(next_page, callback = self.extract)

        next_page = 'https://mag.wodrob.com/hacks/page/' + str(QuoteSpider.page_number2)
        if QuoteSpider.page_number2 <= 10:
            QuoteSpider.page_number2 += 1
            yield response.follow(next_page, callback = self.extract)

        next_page = 'https://mag.wodrob.com/closet-management/page/' + str(QuoteSpider.page_number3)
        if QuoteSpider.page_number3 <= 4:
            QuoteSpider.page_number3 += 1
            yield response.follow(next_page, callback = self.extract)
            

        
    # def parse(self, response):
    #     items = LivemintItem()

    #     all_div = response.css('article')

    #     for div in all_div:
    #         image = div.css("figure a.featured-img-container").xpath('@style').extract_first().replace("background-image:url(", "").replace(");filter:blur(5px)","")
    #         title = div.css("h2.post-title a::text").extract_first()
    #         description = div.css("div.post-excerpt::text").extract_first()
    #         author = div.css("footer div.meta-item a::text")[0].extract()
    #         date = div.css("footer div.meta-item time::text").extract_first()

    #         items['image'] = image
    #         items['title'] = title
    #         items['description'] = description
    #         items['author'] = author
    #         items['date'] = date

    #         yield items

    #         next_page = response.css('div.nav-links a::attr(href)').get()
    #         if next_page is not None:
    #             next_page = response.urljoin(next_page)
    #             yield scrapy.Request(next_page, callback=self.parse)
            # yield {'image':image,'title': title,'description':description,'author':author,'date':date}

        